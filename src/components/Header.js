import React, {Component} from 'react';
import './cssFiles/Header.css';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class Header extends Component {

render() {
  return (
    <div>
      <div className="player-screen__header">
        <div className="player-screen__landscape" title="Background"/>
        <img alt="" src={this.props.profileImage} className="player-screen__avatar"/>
      </div>
      <div>
        <Card className="player-card-name">
          <CardContent>
            <div className="player-card-name_playerName">{this.props.userName}</div>
            <div className="player-card-name_playerPosition">{this.props.subtitle}</div>
          </CardContent>
        </Card>
      </div>
    </div>
  );
}
}


export default Header;