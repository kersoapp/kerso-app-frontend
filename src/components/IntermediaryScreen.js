import React, {Component} from 'react';
import './cssFiles/IntermediaryScreen.css';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import SocialNets from './SocialNets';
import Header from './Header';
import Contact from './Contact';
import Media from './Media';
import ProfileDescription from "./ProfileDescription";

class IntermediaryScreen extends Component {

  render() {
    
    const {user} = this.props;
    
    return (
      <div className="services-screen">
        <Header profileImage={user.profileImage} userName = {user.name}
                subtitle = {user.position}/>
        <Contact/>
        <SocialNets urlFacebook={user.urlSocialNets.facebook} urlTwitter={user.urlSocialNets.twitter}
                    urlInstagram={user.urlSocialNets.instagram} urlLinkedin={user.urlSocialNets.linkedin}
                    urlWS={user.urlSocialNets.whatSapp}/>
        <ProfileDescription userDescription = {user.description}/>
        <Media userMedia = {user}/>
      </div>
    );
  }
}

export default IntermediaryScreen;