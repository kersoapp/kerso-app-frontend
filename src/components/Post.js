import React, {Component} from 'react';
import './cssFiles/Post.css';

class Post extends Component {

  render() {
    return (
      <div className="post">
        <div className="post__img">
          <img src={this.props.postImage} className="img-post"/>
        </div>
        <div className="post__container-info">
          <div className="container-info__description">
            {this.props.postDescription}
          </div>
          <div className="container-info__date">
            {this.props.postTime}
          </div>
          <div className="container-info__options">
            <div className="container-info__likes value-leftmost-likes">
              <img src="/images/like-icon.png" className="like-icon" />
              <div className="likes-amount">{this.props.postLikes}</div>
            </div>
            <div className="container-info__comments value-rightmost-comments">
              <img src="/images/comment-icon.png" className="comment-icon" />
              <div className="comments-amount">{this.props.postComments}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Post;
