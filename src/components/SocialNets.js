import React, {Component} from 'react';
import './cssFiles/SocialNets.css';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class SocialNets extends Component {

  render() {
    return (
      <Card className="player-card-social-net">
        <CardContent className="player-card-social-net__img-container">
          <a href={this.props.urlFacebook}><img src="/images/facebook.png" className ="player-card-social-net__img player-card-social-net__value-leftmost"/></a>
          <a href={this.props.urlTwitter}><img src="/images/twitter.png" className ="player-card-social-net__img"/></a>
          <a href={this.props.urlInstragram}><img src="/images/instagram.png" className ="player-card-social-net__img"/></a>
          <a href={this.props.urlLinkedin}><img src="/images/linkedin.png" className ="player-card-social-net__img"/></a>
          <a href={this.props.urlWS}><img src="/images/whatsapp.png" className ="player-card-social-net__img player-card-social-net__value-rightmost"/></a>
        </CardContent>
      </Card>

    );
  }
}

export default SocialNets;