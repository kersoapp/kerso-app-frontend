import React, {Component} from 'react';
import './cssFiles/Media.css';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Post from './Post';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

class Media extends Component {
  state = {
    value: 0,
    screenWidth: window.innerWidth
  };

  componentDidMount() {
    window.onresize = () => this.setState({screenWidth: window.innerWidth});
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  render() {
    const {screenWidth} = this.state;

    return (
      <Card className="player-card-media">
        <CardContent className="player-card-social-media__media-container">
          <div className="player-card-social-media__content">
            <AppBar position="static" color="default">
              <Tabs value={this.state.value} onChange={this.handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    fullWidth>
                <Tab label="Publicaciones" className="player-card-media__media-bar-value player-card-media__media-bar-posts" />
                <Tab label="Videos" className="player-card-media__media-bar-value"/>
                <Tab label="Fotos" className="player-card-media__media-bar-value"/>
              </Tabs>
            </AppBar>
            <SwipeableViews index={this.state.value} onChangeIndex={this.handleChangeIndex}>
              <div className="media-container__option">
                {this.props.userMedia.posts.map((p, i)=>(
                  <div>
                  <Post postImage={p.img} postDescription = {p.description} postTime={p.time} postLikes={p.likes} postComments={p.comments}/>
                  <div className="media-container__line"/>
                  </div>
                ))}
              </div>
              <div className="media-container__option">
                <div className="player-card-media__grid-container">
                  <GridList cellHeight={180} className="player-card-media__grid-list" cols={screenWidth < 750 ? 2 : screenWidth < 1360 ? 3 : Math.floor(screenWidth / 350)}>
                    {this.props.userMedia.videos.map((tile, i)=> (
                      <GridListTile key={tile + i}>
                        <iframe width="100vw" height="180"
                                src={tile} frameBorder="0"
                                allow="autoplay; encrypted-media" allowFullScreen className={screenWidth < 750 ? "media-container-videos-2" : screenWidth < 1360 ? "media-container-videos-3" : ("media-container-videos-"+Math.floor(screenWidth / 350))}/>
                      </GridListTile>
                    ))}
                  </GridList>
                </div>
              </div>
              <div className="media-container__option">
                <div className="player-card-media__grid-container">
                  <GridList cellHeight={180} className="player-card-media__grid-list" cols={screenWidth < 750 ? 2 : screenWidth < 1360 ? 3 : Math.floor(screenWidth / 350)}>
                    {this.props.userMedia.images.map((tile, i) => (
                      <GridListTile key={tile + i}>
                        <img src={tile} alt={""} className="media-container__image"/>
                        <GridListTileBar
                          title={""}
                          subtitle={<span>by: {this.props.userMedia.name}</span>}
                          actionIcon={
                            <IconButton className="player-card-media__icon">
                              <InfoIcon />
                            </IconButton>
                          }
                        />
                      </GridListTile>
                    ))}
                  </GridList>
                </div>
              </div>
            </SwipeableViews>
          </div>
        </CardContent>
      </Card>
    );
  }
}


export default Media;