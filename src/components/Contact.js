import React, {Component} from 'react';
import './cssFiles/Contact.css';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class Contact extends Component {

  render() {
    return (
      <Card className="player-card-contact">
        <CardContent className="player-card-contact__data">
          <img alt="" src="/images/cv.png" className="player-card-contact__cv player-card-contact__value-leftmost"/>
          <button className="player-card-contact__contact-button">Contacto</button>
          <img alt="" src="/images/stats.png" className="player-card-contact__stats player-card-contact__value-rightmost"/>
        </CardContent>
      </Card>
    );
  }
}


export default Contact;