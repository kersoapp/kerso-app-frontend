import React, {Component} from 'react';
import './cssFiles/PlayerScreen.css';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import SocialNets from './SocialNets';
import Header from './Header';
import Contact from './Contact';
import ProfileDescription from './ProfileDescription';
import Media from './Media';


class PlayerScreen extends Component {

  render() {
    const {player} = this.props;
    return (
      <div className="player-screen">
        <Header profileImage={player.profileImage} userName = {player.name}
        subtitle = {player.position}/>
          <Card className="player-card-info">
            <CardContent className="player-card-info__data">
              <div className="player-card-info__value value-leftmost" >
                <div className="player-data">{player.info.age}</div>
                <div className="player-data-name">Años</div>
              </div>
              <div className="player-card-info__value">
                <div className="player-data">{player.info.height}</div>
                <div className="player-data-name">Metros</div>
              </div>
              <div className="player-card-info__value">
                <div className="player-data">{player.info.weight}</div>
                <div className="player-data-name">Kg</div>
              </div>
              <div className="player-card-info__value value-rightmost">
                <div className="player-data">{player.info.foot}</div>
                <div className="player-data-name">Diestro</div>
              </div>
            </CardContent>
          </Card>
            <Contact/>
            <ProfileDescription userDescription = {player.description}/>
            <SocialNets urlFacebook={player.urlSocialNets.facebook} urlTwitter={player.urlSocialNets.twitter}
                        urlInstagram={player.urlSocialNets.instagram} urlLinkedin={player.urlSocialNets.linkedin}
                        urlWS={player.urlSocialNets.whatSapp}/>
            <Media userMedia = {player}/>
        </div>
    );
  }
}

export default PlayerScreen;