import React, {Component} from 'react';
import './cssFiles/ProfileDescription.css';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class ProfileDescription extends Component {

  render() {
    return (
      <Card className="player-card-description">
        <CardContent>
          <div className="player-card-description__about-name">Perfil</div>
          <div className="player-card-description__about-content">{this.props.userDescription}</div>
        </CardContent>
      </Card>
    );
  }
}


export default ProfileDescription;