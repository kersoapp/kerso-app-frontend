import React, {Component} from 'react';
import './cssFiles/ClubScreen.css';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import SocialNets from './SocialNets';
import Header from './Header';
import Contact from './Contact';
import Media from './Media';

class ClubScreen extends Component {

  render() {
    const {user} = this.props;
    return (
      <div className="club-screen">
        <Header profileImage={user.profileImage} userName = {user.name}
                subtitle = {user.position}/>
        <SocialNets urlFacebook={user.urlSocialNets.facebook} urlTwitter={user.urlSocialNets.twitter}
                    urlInstagram={user.urlSocialNets.instagram} urlLinkedin={user.urlSocialNets.linkedin}
                    urlWS={user.urlSocialNets.whatSapp}/>
        <Contact/>
        <Media userMedia = {user}/>
      </div>
    );
  }
}

export default ClubScreen;