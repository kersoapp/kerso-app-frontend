import React, {Component} from 'react';
import PlayerScreen from './components/PlayerScreen';
import ClubScreen from './components/ClubScreen';
import ServicesScreen from './components/ServicesScreen';

import UniversityServicesScreen from './components/UniversitysServicesScreen';
import IntermediaryScreen from './components/IntermediaryScreen';
import TechnicalStaffScreen from './components/TechnicalStaffScreen';
import LogInPage from './components/LogInPage';
import {firebaseApp} from "./firebase";
import {withStyles} from '@material-ui/core/styles';
import './App.css';
import SponsorsScreen from "./components/SponsorsScreen";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  }
});

class App extends Component {
  state = {
    loggedIn: false,
    currentUser: ""
  };

  componentWillMount() {
    this.auth();
  }

  auth = () => {
    firebaseApp.auth().onAuthStateChanged(user => {
      if (user) {
        console.log(user);
        this.setState({loggedIn: true, currentUser: user.email});
      }
      else {
        this.setState({loggedIn: false});
      }
    })
  };

  render() {
    return (
      <div className="App">
        {this.state.loggedIn ?
          <fragment>
            <header className="App-header">
              <div className="App-header__menu-logo">
                <img src="/images/menu.png" className="app-menu"/>
                <img src="/images/logo.png" className="logo-bar"/>
              </div>
              <img src="/images/search.png" className="app-search"/>
            </header>
            {this.state.currentUser === "edenhazard@gmail.com" ?
              <PlayerScreen player = {players[0]}/>
              :
              this.state.currentUser === "fcbarcelona@gmail.com" ?
                <ClubScreen user = {players[1]}/>
                :
                this.state.currentUser === "helenacorson@gmail.com" ?
                  <ServicesScreen user = {players[2]}/>
                  :
                  this.state.currentUser === "jhuniversity@gmail.com" ?
                    <UniversityServicesScreen user = {players[3]}/>
                    :
                    this.state.currentUser === "carlossilva@gmail.com" ?
                      <IntermediaryScreen user = {players[4]}/>
                      :
                      this.state.currentUser === "raulstevens@gmail.com" ?
                        <TechnicalStaffScreen user = {players[5]}/>
                        :
                        this.state.currentUser === "nike@gmail.com" ?
                          <SponsorsScreen user = {players[6]}/>
                          :
                          ""
            }
          </fragment>
          :
          <LogInPage/>
        }
      </div>
    );
  }
}

export default withStyles(styles)(App);

const players = [
  {
    name: "Eden Hazard",
    profileImage: "/images/player-3.jpg",
    email: "edenhazard@gmail.com",
    type: "player",
    position: "Delantero",
    info: {age: 19, height: 1.80, weight: 76, foot: "D"},
    urlCV: "",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
    urlSocialNets: {
      facebook: "https://www.facebook.com/edenhazard/", twitter: "https://twitter.com/hazardeden10",
      instagram: "https://www.instagram.com/hazdeden_10/?hl=es-la", linkedin: "", whatSapp: ""
    },
    posts: [
      {
        img: "/images/soccer2.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "1 HOUR",
        likes: 123,
        comments: 98
      },
      {
        img: "/images/soccer3.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "2 HOUR",
        likes: 143,
        comments: 34
      },
      {
        img: "/images/soccer4.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "3 HOUR",
        likes: 155,
        comments: 67
      }
    ],
    images: ["/images/player-image1.jpg", "/images/player-image2.jpg", "/images/player-image3.jpg", "/images/player-image4.jpg",
      "/images/player-image5.jpg", "/images/player-image6.jpg"],
    videos: ["https://youtube.com/embed/ZL3Ma_Y2GOg", "https://youtube.com/embed/01TxCcAtEG0",
      "https://youtube.com//embed/CmwYXgkCwjQ", "https://youtube.com/embed/FgKZCP3jX80",
      "https://www.youtube.com/embed/8BLWRt3Yksc"]
  },

  {
    name: "FC Barcelona",
    profileImage: "/images/club-profile.jpg",
    email: "fcbarcelona@gmail.com",
    type: "club",
    year: "1997",
    position: "Spain 1997",
    urlSocialNets: {
      facebook: "https://www.facebook.com/edenhazard/", twitter: "https://twitter.com/hazardeden10",
      instagram: "https://www.instagram.com/hazdeden_10/?hl=es-la", linkedin: "", whatSapp: ""
    },
    players: [
      {img: "/images/soccer2.jpg", name: "player1", position: "Delantero"},
      {img: "/images/soccer3.jpg", name: "player2", position: "Defensa"},
      {img: "/images/soccer4.jpg", name: "player3", position: "Portero"},
      {img: "/images/soccer4.jpg", name: "player3", position: "Delantero"},
      {img: "/images/soccer4.jpg", name: "player3", position: "Defensa"}
    ],
    prices: [
      {img: "/images/soccer2.jpg", name: "player1"},
      {img: "/images/soccer3.jpg", name: "player2"},
      {img: "/images/soccer4.jpg", name: "player3"}
    ],
    posts: [
      {
        img: "/images/club-publication1.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "1 HOUR",
        likes: 123,
        comments: 98
      },
      {
        img: "/images/club-publication2.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "2 HOUR",
        likes: 143,
        comments: 34
      },
      {
        img: "/images/club-publication3.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "3 HOUR",
        likes: 155,
        comments: 67
      }
    ],
    images: ["/images/club-image1.jpg", "/images/club-image2.jpg", "/images/club-image3.jpg", "/images/club-image4.jpg",
      "/images/club-image5.jpg", "/images/club-image6.jpg"],
    videos: ["https://youtube.com/embed/FFF8KQ0xaew", "https://youtube.com/embed/0ap3r7PDjpU",
      "https://youtube.com/embed/wFILCxX7k1A", "https://youtube.com/embed/z6sVRpT10JU",
      "https://youtube.com/embed/pghs62wQI9I", "https://youtube.com/embed/pghs62wQI9I"]
  },
  {
    name: "Laura Wasser",
    profileImage: "/images/services-profile.jpg",
    email: "helenacorson@gmail.com",
    type: "services",
    position: "Abogada",
    country: "Colombia",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
    urlCV: "",
    urlSocialNets: {
      facebook: "https://www.facebook.com/edenhazard/", twitter: "https://twitter.com/hazardeden10",
      instagram: "https://www.instagram.com/hazdeden_10/?hl=es-la", linkedin: "", whatSapp: ""
    },
    experience: [
      {company: "Club Barcelona", position: "Asesoria legal", year: 2017, time: "1 año"},
      {company: "Club Barcelona", position: "Asesoria legal", year: 2016, time: "1 año"},
      {company: "Club Barcelona", position: "Asesoria legal", year: 2013, time: "3 años"}
    ],
    posts: [
      {
        img: "/images/services-post1.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "1 HOUR",
        likes: 123,
        comments: 98
      },
      {
        img: "/images/services-post2.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "2 HOUR",
        likes: 143,
        comments: 34
      },
      {
        img: "/images/services-post3.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "3 HOUR",
        likes: 155,
        comments: 67
      }
    ],
    images: ["/images/services-image1.jpg", "/images/services-image2.jpg", "/images/services-image3.jpg", "/images/services-image4.jpg",
      "/images/services-image5.jpg", "/images/services-image6.jpg"],
    videos: ["https://www.youtube.com/embed/xrOexgz__fw", "https://www.youtube.com/embed/naiGio0eGIw",
      "https://www.youtube.com/embed/dJ9seE1dY1Y", "https://www.youtube.com/embed/Bbl-u2ZUmOY",
      "https://www.youtube.com/embed/dJ9seE1dY1Y"]
  },

  {
    name: "JH University",
    profileImage: "/images/university-profile.jpg",
    email: "jhuniversity@gmail.com",
    type: "university-services",
    position: "Australia",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
    professionalTechnicians: [
      {name: "Marketing en futbol", description: "asdfghjklñ"},
      {name: "Marketing en futbol", description: "sadsfdghjhkj"},
      {name: "Marketing en futbol", description: "asfdsgfdhgfhjgkj"}
    ],
    urlSocialNets: {
      facebook: "https://www.facebook.com/edenhazard/", twitter: "https://twitter.com/hazardeden10",
      instagram: "https://www.instagram.com/hazdeden_10/?hl=es-la", linkedin: "", whatSapp: ""
    },
    posts: [
      {
        img: "/images/soccer2.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "1 HOUR",
        likes: 123,
        comments: 98
      },
      {
        img: "/images/soccer3.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "2 HOUR",
        likes: 143,
        comments: 34
      },
      {
        img: "/images/soccer4.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "3 HOUR",
        likes: 155,
        comments: 67
      }
    ],
    images: ["/images/playerPhoto1.jpg", "/images/playerPhoto2.png", "/images/playerPhoto3.jpg", "/images/playerPhoto4.jpg",
      "/images/playerPhoto5.jpg", "/images/playerPhoto6.jpg"],
    videos: ["https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag", "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag",
      "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag", "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag",
      "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag"]
  },
  {
    name: "Carlos Silva",
    profileImage: "/images/intermediary-profile.jpg",
    email: "carlossilva@gmail.com",
    type: "agent",
    position: "Intermediario",
    country: "Colombia",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
    urlCV: "",
    urlSocialNets: {
      facebook: "https://www.facebook.com/edenhazard/", twitter: "https://twitter.com/hazardeden10",
      instagram: "https://www.instagram.com/hazdeden_10/?hl=es-la", linkedin: "", whatSapp: ""
    },
    representedPlayers: [
      {img: "/images/soccer2.jpg", name: "player1"},
      {img: "/images/soccer3.jpg", name: "player2"},
      {img: "/images/soccer4.jpg", name: "player3"},
      {img: "/images/soccer4.jpg", name: "player3"},
      {img: "/images/soccer4.jpg", name: "player3"}
    ],
    experience: [
      {company: "Club Barcelona", position: "Asesoria legal", year: 2017, time: "1 año"},
      {company: "Club Barcelona", position: "Asesoria legal", year: 2016, time: "1 año"},
      {company: "Club Barcelona", position: "Asesoria legal", year: 2013, time: "3 años"}
    ],
    posts: [
      {
        img: "/images/soccer2.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "1 HOUR",
        likes: 123,
        comments: 98
      },
      {
        img: "/images/soccer3.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "2 HOUR",
        likes: 143,
        comments: 34
      },
      {
        img: "/images/soccer4.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "3 HOUR",
        likes: 155,
        comments: 67
      }
    ],
    images: ["/images/playerPhoto1.jpg", "/images/playerPhoto2.png", "/images/playerPhoto3.jpg", "/images/playerPhoto4.jpg",
      "/images/playerPhoto5.jpg", "/images/playerPhoto6.jpg"],
    videos: ["https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag", "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag",
      "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag", "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag",
      "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag"]
  },
  {
    name: "Raul Stevens",
    profileImage: "/images/technician-profile.jpg",
    email: "raulstevens@gmail.com",
    type: "technicalStaff",
    position:"Cuerpo técnico",
    country: "Colombia",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
    urlCV: "",
    prices: [
      {img: "/images/soccer2.jpg", name: "player1"},
      {img: "/images/soccer3.jpg", name: "player2"},
      {img: "/images/soccer4.jpg", name: "player3"}
    ],
    urlSocialNets: {
      facebook: "https://www.facebook.com/edenhazard/", twitter: "https://twitter.com/hazardeden10",
      instagram: "https://www.instagram.com/hazdeden_10/?hl=es-la", linkedin: "", whatSapp: ""
    },
    experience: [
      {company: "Club Barcelona", position: "Tecnico", year: 2017, time: "1 año"},
      {company: "Club Barcelona", position: "Tecnico", year: 2016, time: "1 año"},
      {company: "Club Barcelona", position: "Tecnico", year: 2013, time: "3 años"}
    ],
    posts: [
      {
        img: "/images/soccer2.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "1 HOUR",
        likes: 123,
        comments: 98
      },
      {
        img: "/images/soccer3.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "2 HOUR",
        likes: 143,
        comments: 34
      },
      {
        img: "/images/soccer4.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "3 HOUR",
        likes: 155,
        comments: 67
      }
    ],
    images: ["/images/playerPhoto1.jpg", "/images/playerPhoto2.png", "/images/playerPhoto3.jpg", "/images/playerPhoto4.jpg",
      "/images/playerPhoto5.jpg", "/images/playerPhoto6.jpg"],
    videos: ["https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag", "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag",
      "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag", "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag",
      "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag"]
  },
  {
    name: "Nike",
    profileImage: "/images/player-3.jpg",
    email: "nike@gmail.com",
    type: "sponsors",
    position: "Patrocinador",
    country: "Beaverton",
    urlSocialNets: {
      facebook: "https://www.facebook.com/edenhazard/", twitter: "https://twitter.com/hazardeden10",
      instagram: "https://www.instagram.com/hazdeden_10/?hl=es-la", linkedin: "", whatSapp: ""
    },
    sponsoredPlayers: [
      {img: "/images/soccer2.jpg", name: "player1"},
      {img: "/images/soccer3.jpg", name: "player2"},
      {img: "/images/soccer4.jpg", name: "player3"},
      {img: "/images/soccer4.jpg", name: "player3"},
      {img: "/images/soccer4.jpg", name: "player3"}
    ],
    posts: [
      {
        img: "/images/soccer2.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "1 HOUR",
        likes: 123,
        comments: 98
      },
      {
        img: "/images/soccer3.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "2 HOUR",
        likes: 143,
        comments: 34
      },
      {
        img: "/images/soccer4.jpg",
        description: "One of the best ways to make a great game. #soccer #game",
        time: "3 HOUR",
        likes: 155,
        comments: 67
      }
    ],
    images: ["/images/playerPhoto1.jpg", "/images/playerPhoto2.png", "/images/playerPhoto3.jpg", "/images/playerPhoto4.jpg",
      "/images/playerPhoto5.jpg", "/images/playerPhoto6.jpg"],
    videos: ["https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag", "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag",
      "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag", "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag",
      "https://www.youtube.com/embed/y7d9VLRO3vc?list=RDsD9_l3oDOag"]
  }
];

