import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyC8Nxc0tppk9OUoh54JbZj3lKyjD1Hi7MU",
  authDomain: "kerso-dac86.firebaseapp.com",
  databaseURL: "https://kerso-dac86.firebaseio.com",
  projectId: "kerso-dac86",
  storageBucket: "",
  messagingSenderId: "603672074104"
};

export const firebaseApp = firebase.initializeApp(config);