const reducer = (state ='State', action) => {
  if(action.type === 'changeState'){
    return action.payload.newState;
  }
  else
    return state;

}
export default reducer;