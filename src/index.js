import React from 'react';
import './index.css';
import App from './App';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './reducers'

const store = createStore(reducers);
//store.dispatch(action);
render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'));

